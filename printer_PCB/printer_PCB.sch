EESchema Schematic File Version 4
LIBS:printer_PCB-cache
EELAYER 26 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 1
Title ""
Date "2018-10-06"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Driver_Motor:Pololu_Breakout_DRV8825 A1
U 1 1 5BAAA32D
P 1750 1640
F 0 "A1" H 1550 840 50  0000 C CNN
F 1 "Pololu_Breakout_DRV8825" H 1750 2480 50  0000 C CNN
F 2 "Module:Pololu_Breakout-16_15.2x20.3mm" H 1950 840 50  0001 L CNN
F 3 "https://www.pololu.com/product/2982" H 1850 1340 50  0001 C CNN
	1    1750 1640
	1    0    0    -1  
$EndComp
$Comp
L Driver_Motor:Pololu_Breakout_DRV8825 A2
U 1 1 5BAAA3E0
P 4040 1730
F 0 "A2" H 4040 1340 50  0000 C CNN
F 1 "Pololu_Breakout_DRV8825" H 4270 2540 50  0000 C CNN
F 2 "Module:Pololu_Breakout-16_15.2x20.3mm" H 4240 930 50  0001 L CNN
F 3 "https://www.pololu.com/product/2982" H 4140 1430 50  0001 C CNN
	1    4040 1730
	1    0    0    -1  
$EndComp
$Comp
L Driver_Motor:Pololu_Breakout_DRV8825 A4
U 1 1 5BAAA48D
P 8720 1670
F 0 "A4" H 8520 870 50  0000 C CNN
F 1 "Pololu_Breakout_DRV8825" H 8720 2530 50  0000 C CNN
F 2 "Module:Pololu_Breakout-16_15.2x20.3mm" H 8920 870 50  0001 L CNN
F 3 "https://www.pololu.com/product/2982" H 8820 1370 50  0001 C CNN
	1    8720 1670
	1    0    0    -1  
$EndComp
$Comp
L Driver_Motor:Pololu_Breakout_DRV8825 A5
U 1 1 5BAAA4FA
P 10890 1720
F 0 "A5" H 10890 1350 50  0000 C CNN
F 1 "Pololu_Breakout_DRV8825" H 10890 2570 50  0000 C CNN
F 2 "Module:Pololu_Breakout-16_15.2x20.3mm" H 11090 920 50  0001 L CNN
F 3 "https://www.pololu.com/product/2982" H 10990 1420 50  0001 C CNN
	1    10890 1720
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x03_Female J1
U 1 1 5BAAAEFD
P 980 8240
F 0 "J1" H 874 7915 50  0000 C CNN
F 1 "Conn_01x03_Female" H 874 8006 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x03_P2.54mm_Vertical" H 980 8240 50  0001 C CNN
F 3 "~" H 980 8240 50  0001 C CNN
	1    980  8240
	-1   0    0    1   
$EndComp
Wire Wire Line
	1180 8140 1580 8140
Wire Wire Line
	1180 8240 1580 8240
Wire Wire Line
	1180 8340 1580 8340
Text Label 1580 8240 2    50   ~ 0
GND
Text Label 1580 8140 2    50   ~ 0
12V
Text Label 1580 8340 2    50   ~ 0
5V
Wire Wire Line
	1750 1040 1750 990 
Wire Wire Line
	1600 990  1750 990 
Wire Wire Line
	1750 990  2250 990 
Text Label 1600 990  0    50   ~ 0
12V
Text Label 3890 1080 0    50   ~ 0
12V
Wire Wire Line
	3890 1080 4040 1080
Wire Wire Line
	4040 1080 4540 1080
Wire Wire Line
	4040 1080 4040 1130
Text Label 6200 980  0    50   ~ 0
12V
Wire Wire Line
	6200 980  6350 980 
Wire Wire Line
	6350 980  6850 980 
Wire Wire Line
	6350 980  6350 1030
Text Label 8570 1020 0    50   ~ 0
12V
Wire Wire Line
	8570 1020 8720 1020
Wire Wire Line
	8720 1020 9220 1020
Wire Wire Line
	8720 1020 8720 1070
Wire Wire Line
	2150 1840 2350 1840
Wire Wire Line
	2350 1840 2350 1790
Wire Wire Line
	2350 1890 2350 1940
Wire Wire Line
	2350 1940 2150 1940
Wire Wire Line
	4440 1930 4640 1930
Wire Wire Line
	4640 1930 4640 1880
Wire Wire Line
	4640 1980 4640 2030
Wire Wire Line
	4640 2030 4440 2030
Wire Wire Line
	6750 1830 6950 1830
Wire Wire Line
	6950 1830 6950 1780
Wire Wire Line
	6950 1880 6950 1930
Wire Wire Line
	6950 1930 6750 1930
Wire Wire Line
	9120 1870 9320 1870
Wire Wire Line
	9320 1870 9320 1820
Wire Wire Line
	9320 1920 9320 1970
Wire Wire Line
	9320 1970 9120 1970
Wire Wire Line
	11290 1920 11490 1920
Wire Wire Line
	11490 1920 11490 1870
Wire Wire Line
	11490 1970 11490 2020
Wire Wire Line
	11490 2020 11290 2020
Text Label 10740 1070 0    50   ~ 0
12V
Wire Wire Line
	10740 1070 10890 1070
Wire Wire Line
	10890 1070 11390 1070
Wire Wire Line
	10890 1070 10890 1120
Wire Wire Line
	1750 2440 1750 2540
Wire Wire Line
	1750 2540 1800 2540
Wire Wire Line
	1800 2540 1850 2540
Wire Wire Line
	1850 2540 1850 2440
Wire Wire Line
	1800 2540 1800 2750
Text Label 1800 2750 1    50   ~ 0
GND
Wire Wire Line
	4040 2530 4040 2630
Wire Wire Line
	4040 2630 4090 2630
Wire Wire Line
	4090 2630 4140 2630
Wire Wire Line
	4140 2630 4140 2530
Wire Wire Line
	4090 2630 4090 2830
Connection ~ 4090 2630
Text Label 4090 2830 1    50   ~ 0
GND
Wire Wire Line
	6350 2430 6350 2530
Wire Wire Line
	6350 2530 6400 2530
Wire Wire Line
	6400 2530 6450 2530
Wire Wire Line
	6450 2530 6450 2430
Wire Wire Line
	6400 2530 6400 2730
Connection ~ 6400 2530
Text Label 6400 2730 1    50   ~ 0
GND
Wire Wire Line
	8720 2470 8720 2570
Wire Wire Line
	8720 2570 8770 2570
Wire Wire Line
	8770 2570 8820 2570
Wire Wire Line
	8820 2570 8820 2470
Wire Wire Line
	8770 2570 8770 2770
Connection ~ 8770 2570
Text Label 8770 2770 1    50   ~ 0
GND
Wire Wire Line
	10890 2520 10890 2620
Wire Wire Line
	10890 2620 10940 2620
Wire Wire Line
	10940 2620 10990 2620
Wire Wire Line
	10990 2620 10990 2520
Wire Wire Line
	10940 2620 10940 2820
Connection ~ 10940 2620
Text Label 10940 2820 1    50   ~ 0
GND
Connection ~ 6350 980 
Wire Wire Line
	6850 1280 6850 1330
Text Label 6850 1330 0    50   ~ 0
GND
Wire Wire Line
	4540 1380 4540 1430
Text Label 4540 1430 0    50   ~ 0
GND
Connection ~ 4040 1080
Wire Wire Line
	9220 1320 9220 1370
Text Label 9220 1370 0    50   ~ 0
GND
Connection ~ 8720 1020
Wire Wire Line
	11390 1370 11390 1420
Text Label 11390 1420 0    50   ~ 0
GND
Connection ~ 10890 1070
Wire Wire Line
	2250 1290 2250 1340
Text Label 2250 1340 0    50   ~ 0
GND
Connection ~ 1750 990 
Text Notes 1650 770  0    197  ~ 39
X
Text Notes 3920 810  0    197  ~ 39
Y
Text Notes 6150 740  0    197  ~ 39
Z1
Text Notes 8510 760  0    197  ~ 39
Z2
Text Notes 10790 820  0    197  ~ 39
E
$Comp
L Transistor_FET:IRLZ34N Q1
U 1 1 5BAC6F88
P 3440 6390
F 0 "Q1" H 3645 6436 50  0000 L CNN
F 1 "IRL540N" H 3645 6345 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 3690 6315 50  0001 L CIN
F 3 "http://www.infineon.com/dgdl/irlz34npbf.pdf?fileId=5546d462533600a40153567206892720" H 3440 6390 50  0001 L CNN
	1    3440 6390
	1    0    0    -1  
$EndComp
Wire Wire Line
	3540 6590 3540 6840
Wire Wire Line
	3540 6840 3540 6940
Wire Wire Line
	2940 6390 3040 6390
Wire Wire Line
	3040 6390 3240 6390
Wire Wire Line
	3040 6390 3040 6490
$Comp
L Device:R R2
U 1 1 5BACD08F
P 3040 6640
F 0 "R2" V 3130 6580 50  0000 L CNN
F 1 "100k" V 2960 6550 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 2970 6640 50  0001 C CNN
F 3 "~" H 3040 6640 50  0001 C CNN
	1    3040 6640
	1    0    0    -1  
$EndComp
Wire Wire Line
	3040 6790 3040 6840
Wire Wire Line
	3040 6840 3540 6840
Connection ~ 3040 6390
$Comp
L Device:R R1
U 1 1 5BAD3B0A
P 2790 6390
F 0 "R1" V 2880 6390 50  0000 C CNN
F 1 "10R" V 2710 6390 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 2720 6390 50  0001 C CNN
F 3 "~" H 2790 6390 50  0001 C CNN
	1    2790 6390
	0    1    1    0   
$EndComp
Wire Wire Line
	2640 6390 2490 6390
Text Label 3540 5690 0    50   ~ 0
12V
Wire Wire Line
	3540 5690 3540 5740
Wire Wire Line
	3540 5740 3540 5940
Wire Wire Line
	3540 6040 3540 6140
Wire Wire Line
	3540 6140 3540 6190
Connection ~ 3540 5740
$Comp
L Device:R R3
U 1 1 5BAE2D20
P 3090 5890
F 0 "R3" V 3170 5830 50  0000 L CNN
F 1 "560R" V 3010 5800 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 3020 5890 50  0001 C CNN
F 3 "~" H 3090 5890 50  0001 C CNN
	1    3090 5890
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D2
U 1 1 5BAE2D9E
P 3340 6140
F 0 "D2" H 3340 6240 50  0000 C CNN
F 1 "LED" H 3340 6050 50  0000 C CNN
F 2 "LED_SMD:LED_0603_1608Metric" H 3340 6140 50  0001 C CNN
F 3 "~" H 3340 6140 50  0001 C CNN
	1    3340 6140
	-1   0    0    1   
$EndComp
Wire Wire Line
	3090 5740 3540 5740
Wire Wire Line
	3090 6040 3090 6140
Wire Wire Line
	3090 6140 3190 6140
Wire Wire Line
	3540 6140 3490 6140
Connection ~ 3540 6140
Connection ~ 3540 6840
Text Label 3540 6940 0    50   ~ 0
GND
Text Notes 3140 5540 0    118  ~ 24
heater
$Comp
L Transistor_FET:IRLZ34N Q2
U 1 1 5BB226FC
P 5580 6410
F 0 "Q2" H 5785 6456 50  0000 L CNN
F 1 "IRL540N" H 5785 6365 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 5830 6335 50  0001 L CIN
F 3 "http://www.infineon.com/dgdl/irlz34npbf.pdf?fileId=5546d462533600a40153567206892720" H 5580 6410 50  0001 L CNN
	1    5580 6410
	1    0    0    -1  
$EndComp
Wire Wire Line
	5680 6610 5680 6860
Wire Wire Line
	5680 6860 5680 6960
Wire Wire Line
	5080 6410 5180 6410
Wire Wire Line
	5180 6410 5380 6410
Wire Wire Line
	5180 6410 5180 6510
$Comp
L Device:R R7
U 1 1 5BB22705
P 5180 6660
F 0 "R7" V 5270 6600 50  0000 L CNN
F 1 "100k" V 5100 6570 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 5110 6660 50  0001 C CNN
F 3 "~" H 5180 6660 50  0001 C CNN
	1    5180 6660
	1    0    0    -1  
$EndComp
Wire Wire Line
	5180 6810 5180 6860
Wire Wire Line
	5180 6860 5680 6860
Connection ~ 5180 6410
$Comp
L Device:R R6
U 1 1 5BB2270F
P 4930 6410
F 0 "R6" V 5020 6410 50  0000 C CNN
F 1 "10R" V 4850 6410 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 4860 6410 50  0001 C CNN
F 3 "~" H 4930 6410 50  0001 C CNN
	1    4930 6410
	0    1    1    0   
$EndComp
Wire Wire Line
	4780 6410 4630 6410
Text Label 5680 5710 0    50   ~ 0
12V
Wire Wire Line
	5680 5710 5680 5760
Wire Wire Line
	5680 5760 5680 5960
Wire Wire Line
	5680 6060 5680 6160
Wire Wire Line
	5680 6160 5680 6210
Connection ~ 5680 5760
$Comp
L Device:R R8
U 1 1 5BB22721
P 5230 5910
F 0 "R8" V 5310 5860 50  0000 L CNN
F 1 "560R" V 5150 5810 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 5160 5910 50  0001 C CNN
F 3 "2861458" H 5230 5910 50  0001 C CNN
	1    5230 5910
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D3
U 1 1 5BB22727
P 5480 6160
F 0 "D3" H 5480 6270 50  0000 C CNN
F 1 "LED" H 5480 6070 50  0000 C CNN
F 2 "LED_SMD:LED_0603_1608Metric" H 5480 6160 50  0001 C CNN
F 3 "2290332" H 5480 6160 50  0001 C CNN
	1    5480 6160
	-1   0    0    1   
$EndComp
Wire Wire Line
	5230 5760 5680 5760
Wire Wire Line
	5230 6060 5230 6160
Wire Wire Line
	5230 6160 5330 6160
Wire Wire Line
	5680 6160 5630 6160
Connection ~ 5680 6160
Connection ~ 5680 6860
Text Label 5680 6960 0    50   ~ 0
GND
Text Notes 5300 5540 0    118  ~ 24
fan
Wire Wire Line
	2870 3970 3380 3970
Wire Wire Line
	2870 4070 3070 4070
Wire Wire Line
	3070 4070 3380 4070
Text Label 3380 3970 2    50   ~ 0
5V
Text Label 3380 4070 2    50   ~ 0
GND
Wire Wire Line
	2870 4170 2910 4170
Wire Wire Line
	2910 4170 3380 4170
Text Label 3380 4170 2    60   ~ 0
xmin
Wire Wire Line
	3070 4070 3070 4110
Connection ~ 3070 4070
$Comp
L Device:C C2
U 1 1 5BB261D3
P 3070 4260
F 0 "C2" H 3180 4260 50  0000 L CNN
F 1 "0.1uF" H 3095 4160 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3108 4110 50  0001 C CNN
F 3 "" H 3070 4260 50  0001 C CNN
	1    3070 4260
	1    0    0    -1  
$EndComp
Wire Wire Line
	2910 4170 2910 4410
Wire Wire Line
	2910 4410 3070 4410
Connection ~ 2910 4170
Wire Wire Line
	2870 4650 3380 4650
Wire Wire Line
	2870 4750 3070 4750
Wire Wire Line
	3070 4750 3380 4750
Text Label 3380 4650 2    50   ~ 0
5V
Text Label 3380 4750 2    50   ~ 0
GND
Wire Wire Line
	2870 4850 2910 4850
Wire Wire Line
	2910 4850 3380 4850
Text Label 3380 4850 2    60   ~ 0
xmax
Wire Wire Line
	3070 4750 3070 4790
Connection ~ 3070 4750
$Comp
L Device:C C3
U 1 1 5BB2658E
P 3070 4940
F 0 "C3" H 3180 4940 50  0000 L CNN
F 1 "0.1uF" H 3095 4840 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3108 4790 50  0001 C CNN
F 3 "" H 3070 4940 50  0001 C CNN
	1    3070 4940
	1    0    0    -1  
$EndComp
Wire Wire Line
	2910 4850 2910 5090
Wire Wire Line
	2910 5090 3070 5090
Connection ~ 2910 4850
Wire Wire Line
	4190 3970 4700 3970
Wire Wire Line
	4190 4070 4390 4070
Wire Wire Line
	4390 4070 4700 4070
Text Label 4700 3970 2    50   ~ 0
5V
Text Label 4700 4070 2    50   ~ 0
GND
Wire Wire Line
	4190 4170 4230 4170
Wire Wire Line
	4230 4170 4700 4170
Text Label 4700 4170 2    60   ~ 0
ymin
Wire Wire Line
	4390 4070 4390 4110
Connection ~ 4390 4070
$Comp
L Device:C C7
U 1 1 5BB26A8A
P 4390 4260
F 0 "C7" H 4500 4260 50  0000 L CNN
F 1 "0.1uF" H 4415 4160 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4428 4110 50  0001 C CNN
F 3 "" H 4390 4260 50  0001 C CNN
	1    4390 4260
	1    0    0    -1  
$EndComp
Wire Wire Line
	4230 4170 4230 4410
Wire Wire Line
	4230 4410 4390 4410
Connection ~ 4230 4170
Wire Wire Line
	4190 4650 4700 4650
Wire Wire Line
	4190 4750 4390 4750
Wire Wire Line
	4390 4750 4700 4750
Text Label 4700 4650 2    50   ~ 0
5V
Text Label 4700 4750 2    50   ~ 0
GND
Wire Wire Line
	4190 4850 4230 4850
Wire Wire Line
	4230 4850 4700 4850
Text Label 4700 4850 2    60   ~ 0
ymax
Wire Wire Line
	4390 4750 4390 4790
Connection ~ 4390 4750
$Comp
L Device:C C8
U 1 1 5BB26AA1
P 4390 4940
F 0 "C8" H 4490 4940 50  0000 L CNN
F 1 "0.1uF" H 4415 4840 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4428 4790 50  0001 C CNN
F 3 "" H 4390 4940 50  0001 C CNN
	1    4390 4940
	1    0    0    -1  
$EndComp
Wire Wire Line
	4230 4850 4230 5090
Wire Wire Line
	4230 5090 4390 5090
Connection ~ 4230 4850
Wire Wire Line
	5540 3970 6050 3970
Wire Wire Line
	5540 4070 5740 4070
Wire Wire Line
	5740 4070 6050 4070
Text Label 6050 3970 2    50   ~ 0
5V
Text Label 6050 4070 2    50   ~ 0
GND
Wire Wire Line
	5540 4170 5580 4170
Wire Wire Line
	5580 4170 6050 4170
Text Label 6050 4170 2    60   ~ 0
zmin
Wire Wire Line
	5740 4070 5740 4110
Connection ~ 5740 4070
$Comp
L Device:C C10
U 1 1 5BB26FDE
P 5740 4260
F 0 "C10" H 5850 4260 50  0000 L CNN
F 1 "0.1uF" H 5765 4160 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5778 4110 50  0001 C CNN
F 3 "" H 5740 4260 50  0001 C CNN
	1    5740 4260
	1    0    0    -1  
$EndComp
Wire Wire Line
	5580 4170 5580 4410
Wire Wire Line
	5580 4410 5740 4410
Connection ~ 5580 4170
Wire Wire Line
	5540 4650 6050 4650
Wire Wire Line
	5540 4750 5740 4750
Wire Wire Line
	5740 4750 6050 4750
Text Label 6050 4650 2    50   ~ 0
5V
Text Label 6050 4750 2    50   ~ 0
GND
Wire Wire Line
	5540 4850 5580 4850
Wire Wire Line
	5580 4850 6050 4850
Text Label 6050 4850 2    60   ~ 0
zmax
Wire Wire Line
	5740 4750 5740 4790
Connection ~ 5740 4750
$Comp
L Device:C C11
U 1 1 5BB26FF5
P 5740 4940
F 0 "C11" H 5860 4940 50  0000 L CNN
F 1 "0.1uF" H 5765 4840 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5778 4790 50  0001 C CNN
F 3 "" H 5740 4940 50  0001 C CNN
	1    5740 4940
	1    0    0    -1  
$EndComp
Wire Wire Line
	5580 4850 5580 5090
Wire Wire Line
	5580 5090 5740 5090
Connection ~ 5580 4850
Wire Wire Line
	7850 4320 8080 4320
Wire Wire Line
	8080 4320 8240 4320
Wire Wire Line
	8240 4320 8700 4320
$Comp
L Device:R R10
U 1 1 5BB27665
P 8080 4170
F 0 "R10" V 8160 4170 50  0000 C CNN
F 1 "4.7k" V 8000 4170 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 8010 4170 50  0001 C CNN
F 3 "" H 8080 4170 50  0001 C CNN
	1    8080 4170
	1    0    0    -1  
$EndComp
Wire Wire Line
	8080 4020 8080 3980
Text Label 8080 3980 2    60   ~ 0
5V_from_arduino
Wire Wire Line
	8700 4420 8700 4680
Wire Wire Line
	8700 4680 8700 4820
Text Label 8700 4820 2    50   ~ 0
GND
Connection ~ 8080 4320
Wire Wire Line
	8240 4320 8240 4380
Connection ~ 8240 4320
Wire Wire Line
	8240 4680 8700 4680
Connection ~ 8700 4680
Text Label 7850 4320 2    60   ~ 0
thermometer1
Wire Wire Line
	9730 4330 9960 4330
Wire Wire Line
	9960 4330 10120 4330
Wire Wire Line
	10120 4330 10580 4330
$Comp
L Device:R R11
U 1 1 5BB2A5BE
P 9960 4180
F 0 "R11" V 10040 4180 50  0000 C CNN
F 1 "4.7k" V 9880 4180 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 9890 4180 50  0001 C CNN
F 3 "" H 9960 4180 50  0001 C CNN
	1    9960 4180
	1    0    0    -1  
$EndComp
Wire Wire Line
	9960 4030 9960 3990
Wire Wire Line
	10580 4430 10580 4690
Wire Wire Line
	10580 4690 10580 4830
Text Label 10580 4830 2    50   ~ 0
GND
Connection ~ 9960 4330
Wire Wire Line
	10120 4330 10120 4390
Connection ~ 10120 4330
Wire Wire Line
	10120 4690 10580 4690
Connection ~ 10580 4690
Text Label 9730 4330 2    60   ~ 0
thermometer2
Wire Wire Line
	7840 5110 8070 5110
Wire Wire Line
	8070 5110 8230 5110
Wire Wire Line
	8230 5110 8690 5110
$Comp
L Device:R R9
U 1 1 5BB2A994
P 8070 4960
F 0 "R9" V 8150 4960 50  0000 C CNN
F 1 "4.7k" V 7990 4960 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 8000 4960 50  0001 C CNN
F 3 "" H 8070 4960 50  0001 C CNN
	1    8070 4960
	1    0    0    -1  
$EndComp
Wire Wire Line
	8070 4810 8070 4770
Wire Wire Line
	8690 5210 8690 5470
Wire Wire Line
	8690 5470 8690 5610
Text Label 8690 5610 2    50   ~ 0
GND
Connection ~ 8070 5110
Wire Wire Line
	8230 5110 8230 5170
Connection ~ 8230 5110
Wire Wire Line
	8230 5470 8690 5470
Connection ~ 8690 5470
Text Label 7840 5110 2    60   ~ 0
thermometer3
Text Notes 2580 3740 0    118  ~ 24
endstops
Text Notes 8450 3820 0    118  ~ 24
thermistors
Text Notes 3170 7450 0    118  ~ 24
power
$Comp
L Regulator_Linear:LM317_3PinPackage U1
U 1 1 5BB3D131
P 3410 7870
F 0 "U1" H 3410 8112 50  0000 C CNN
F 1 "LM317_3PinPackage" H 3410 8021 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 3410 8120 50  0001 C CIN
F 3 "http://www.ti.com/lit/ds/symlink/lm317.pdf" H 3410 7870 50  0001 C CNN
	1    3410 7870
	1    0    0    -1  
$EndComp
Wire Wire Line
	2660 7870 2870 7870
Wire Wire Line
	3410 8170 3410 8270
Wire Wire Line
	3710 7870 3780 7870
Text Label 4310 7870 2    50   ~ 0
5V
Text Label 9960 3990 2    60   ~ 0
5V_from_arduino
Text Label 8070 4770 2    60   ~ 0
5V_from_arduino
Wire Wire Line
	2870 7870 2870 8120
Connection ~ 2870 7870
Wire Wire Line
	2870 7870 3110 7870
Wire Wire Line
	4070 8120 4070 7870
Connection ~ 4070 7870
Wire Wire Line
	4070 7870 4310 7870
Wire Wire Line
	3780 7870 3780 7970
Connection ~ 3780 7870
Wire Wire Line
	3780 7870 4070 7870
$Comp
L Device:R R5
U 1 1 5BBA5D11
P 3780 8120
F 0 "R5" V 3860 8070 50  0000 L CNN
F 1 "240R" V 3700 8020 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 3710 8120 50  0001 C CNN
F 3 "~" H 3780 8120 50  0001 C CNN
	1    3780 8120
	1    0    0    -1  
$EndComp
Wire Wire Line
	3410 8270 3780 8270
Wire Wire Line
	3410 8270 3410 8320
Connection ~ 3410 8270
Wire Wire Line
	4070 8670 4070 8420
$Comp
L Device:R R4
U 1 1 5BBC7294
P 3410 8470
F 0 "R4" V 3490 8420 50  0000 L CNN
F 1 "720R" V 3330 8370 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 3340 8470 50  0001 C CNN
F 3 "~" H 3410 8470 50  0001 C CNN
	1    3410 8470
	1    0    0    -1  
$EndComp
Wire Wire Line
	2870 8420 2870 8670
Wire Wire Line
	2870 8670 3410 8670
Wire Wire Line
	3410 8620 3410 8670
Wire Wire Line
	3410 8670 4070 8670
Connection ~ 3410 8670
Text Label 3410 8780 0    50   ~ 0
GND
Wire Wire Line
	3410 8670 3410 8780
Text Notes 12990 790  0    118  ~ 24
arduino connectors
$Comp
L Connector_Generic:Conn_01x03 J2
U 1 1 5BC9BF56
P 2670 4070
F 0 "J2" H 2670 4270 50  0000 C CNN
F 1 "Conn_01x03" V 2770 4070 50  0000 C CNN
F 2 "footprint:PinSocket_1x03_P2.54mm_Vertical" H 2670 4070 50  0001 C CNN
F 3 "~" H 2670 4070 50  0001 C CNN
	1    2670 4070
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J3
U 1 1 5BC9D4B3
P 2670 4750
F 0 "J3" H 2670 4950 50  0000 C CNN
F 1 "Conn_01x03" V 2770 4750 50  0000 C CNN
F 2 "footprint:PinSocket_1x03_P2.54mm_Vertical" H 2670 4750 50  0001 C CNN
F 3 "~" H 2670 4750 50  0001 C CNN
	1    2670 4750
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J5
U 1 1 5BC9D567
P 3990 4070
F 0 "J5" H 3990 4270 50  0000 C CNN
F 1 "Conn_01x03" V 4090 4070 50  0000 C CNN
F 2 "footprint:PinSocket_1x03_P2.54mm_Vertical" H 3990 4070 50  0001 C CNN
F 3 "~" H 3990 4070 50  0001 C CNN
	1    3990 4070
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J6
U 1 1 5BC9D79D
P 3990 4750
F 0 "J6" H 3990 4950 50  0000 C CNN
F 1 "Conn_01x03" V 4090 4750 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x03_P2.54mm_Vertical" H 3990 4750 50  0001 C CNN
F 3 "~" H 3990 4750 50  0001 C CNN
	1    3990 4750
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J8
U 1 1 5BC9DF7C
P 5340 4070
F 0 "J8" H 5340 4270 50  0000 C CNN
F 1 "Conn_01x03" V 5450 4070 50  0000 C CNN
F 2 "footprint:PinSocket_1x03_P2.54mm_Vertical" H 5340 4070 50  0001 C CNN
F 3 "~" H 5340 4070 50  0001 C CNN
	1    5340 4070
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J9
U 1 1 5BC9E030
P 5340 4750
F 0 "J9" H 5340 4950 50  0000 C CNN
F 1 "Conn_01x03" V 5450 4750 50  0000 C CNN
F 2 "footprint:PinSocket_1x03_P2.54mm_Vertical" H 5340 4750 50  0001 C CNN
F 3 "~" H 5340 4750 50  0001 C CNN
	1    5340 4750
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J4
U 1 1 5BCA1C1B
P 3740 5940
F 0 "J4" H 3690 5750 50  0000 L CNN
F 1 "Conn_01x02" H 3540 6050 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x02_P2.54mm_Vertical" H 3740 5940 50  0001 C CNN
F 3 "~" H 3740 5940 50  0001 C CNN
	1    3740 5940
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J11
U 1 1 5BCA1DC6
P 5880 5960
F 0 "J11" H 5810 5770 50  0000 L CNN
F 1 "Conn_01x02" H 5690 6080 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x02_P2.54mm_Vertical" H 5880 5960 50  0001 C CNN
F 3 "~" H 5880 5960 50  0001 C CNN
	1    5880 5960
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J14
U 1 1 5BCA4ECF
P 8900 4320
F 0 "J14" H 8830 4120 50  0000 L CNN
F 1 "Conn_01x02" H 8650 4440 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x02_P2.54mm_Vertical" H 8900 4320 50  0001 C CNN
F 3 "~" H 8900 4320 50  0001 C CNN
	1    8900 4320
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J17
U 1 1 5BCA528D
P 10780 4330
F 0 "J17" H 10710 4120 50  0000 L CNN
F 1 "Conn_01x02" H 10550 4460 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x02_P2.54mm_Vertical" H 10780 4330 50  0001 C CNN
F 3 "~" H 10780 4330 50  0001 C CNN
	1    10780 4330
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J13
U 1 1 5BCA60FD
P 8890 5110
F 0 "J13" H 8820 4900 50  0000 L CNN
F 1 "Conn_01x02" H 8670 5260 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x02_P2.54mm_Vertical" H 8890 5110 50  0001 C CNN
F 3 "~" H 8890 5110 50  0001 C CNN
	1    8890 5110
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J16
U 1 1 5BCA8C69
P 11690 1770
F 0 "J16" H 11770 1762 50  0000 L CNN
F 1 "Conn_01x04" H 11460 1970 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x04_P2.54mm_Vertical" H 11690 1770 50  0001 C CNN
F 3 "~" H 11690 1770 50  0001 C CNN
	1    11690 1770
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J12
U 1 1 5BCA94C5
P 7150 1680
F 0 "J12" H 7230 1672 50  0000 L CNN
F 1 "Conn_01x04" H 6900 1300 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x04_P2.54mm_Vertical" H 7150 1680 50  0001 C CNN
F 3 "~" H 7150 1680 50  0001 C CNN
	1    7150 1680
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J10
U 1 1 5BCAA16F
P 4840 1780
F 0 "J10" H 4920 1772 50  0000 L CNN
F 1 "Conn_01x04" H 4670 2010 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x04_P2.54mm_Vertical" H 4840 1780 50  0001 C CNN
F 3 "~" H 4840 1780 50  0001 C CNN
	1    4840 1780
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J7
U 1 1 5BCAA493
P 2550 1690
F 0 "J7" H 2310 1640 50  0000 L CNN
F 1 "Conn_01x04" H 2280 1910 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x04_P2.54mm_Vertical" H 2550 1690 50  0001 C CNN
F 3 "~" H 2550 1690 50  0001 C CNN
	1    2550 1690
	1    0    0    -1  
$EndComp
Wire Wire Line
	1350 1740 1110 1740
Text Label 1110 1740 0    50   ~ 0
xstep
Wire Wire Line
	1350 1840 1110 1840
Text Label 1110 1840 0    50   ~ 0
xdir
Wire Wire Line
	3640 1830 3440 1830
Text Label 3440 1830 0    50   ~ 0
ystep
Wire Wire Line
	3640 1930 3440 1930
Text Label 3440 1930 0    50   ~ 0
ydir
Text Label 590  1640 0    50   ~ 0
xen
Text Label 2900 1730 0    50   ~ 0
yen
Wire Wire Line
	5950 1730 5750 1730
Text Label 5750 1730 0    50   ~ 0
zstep
Text Label 5180 1630 0    50   ~ 0
zen
Wire Wire Line
	5950 1830 5750 1830
Text Label 5750 1830 0    50   ~ 0
zdir
Text Label 7600 1670 0    50   ~ 0
zen
Wire Wire Line
	8320 1770 8110 1770
Wire Wire Line
	8320 1870 8110 1870
Text Label 8110 1770 0    50   ~ 0
zstep
Text Label 8110 1870 0    50   ~ 0
zdir
Text Label 9820 1720 0    50   ~ 0
een
Wire Wire Line
	10490 1820 10300 1820
Text Label 10300 1820 0    50   ~ 0
estep
Wire Wire Line
	10490 1920 10300 1920
Text Label 10300 1920 0    50   ~ 0
edir
Text Label 2730 9270 0    50   ~ 0
12V
Wire Wire Line
	2730 9270 2970 9270
$Comp
L pspice:DIODE D1
U 1 1 5BD5C1A8
P 3170 9270
F 0 "D1" H 3170 9090 50  0000 C CNN
F 1 "DIODE" H 3170 9444 50  0000 C CNN
F 2 "Diode_THT:D_DO-15_P15.24mm_Horizontal" H 3170 9270 50  0001 C CNN
F 3 "" H 3170 9270 50  0001 C CNN
	1    3170 9270
	1    0    0    -1  
$EndComp
Wire Wire Line
	3770 9270 3770 9360
Wire Wire Line
	3770 9660 3770 9760
Text Label 3770 9760 0    50   ~ 0
GND
Text Label 2660 7870 0    50   ~ 0
12V-Dv
$Comp
L Device:Polyfuse_Small F1
U 1 1 5BE63858
P 3620 9270
F 0 "F1" V 3720 9270 50  0000 C CNN
F 1 "Ptc" V 3506 9270 50  0000 C CNN
F 2 "Fuse:Fuse_1812_4532Metric" H 3670 9070 50  0001 L CNN
F 3 "1345924" H 3620 9270 50  0001 C CNN
	1    3620 9270
	0    1    1    0   
$EndComp
Wire Wire Line
	3720 9270 3770 9270
Text Label 2490 6390 2    50   ~ 0
heater1
Text Label 4630 6410 2    50   ~ 0
fan1
$Comp
L Device:CP C12
U 1 1 5BFF3BB9
P 6850 1130
F 0 "C12" H 6968 1176 50  0000 L CNN
F 1 "100uF" H 6968 1085 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_6.3x7.7" H 6888 980 50  0001 C CNN
F 3 "~" H 6850 1130 50  0001 C CNN
	1    6850 1130
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C5
U 1 1 5BFF3CCC
P 2250 1140
F 0 "C5" H 2368 1186 50  0000 L CNN
F 1 "100uF" H 2368 1095 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_6.3x7.7" H 2288 990 50  0001 C CNN
F 3 "~" H 2250 1140 50  0001 C CNN
	1    2250 1140
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C9
U 1 1 5BFF4266
P 4540 1230
F 0 "C9" H 4658 1276 50  0000 L CNN
F 1 "100uF" H 4658 1185 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_6.3x7.7" H 4578 1080 50  0001 C CNN
F 3 "~" H 4540 1230 50  0001 C CNN
	1    4540 1230
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C15
U 1 1 5BFF55EE
P 9220 1170
F 0 "C15" H 9338 1216 50  0000 L CNN
F 1 "100uF" H 9338 1125 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_6.3x7.7" H 9258 1020 50  0001 C CNN
F 3 "~" H 9220 1170 50  0001 C CNN
	1    9220 1170
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C17
U 1 1 5BFF56C4
P 11390 1220
F 0 "C17" H 11508 1266 50  0000 L CNN
F 1 "100uF" H 11508 1175 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_6.3x7.7" H 11428 1070 50  0001 C CNN
F 3 "~" H 11390 1220 50  0001 C CNN
	1    11390 1220
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C4
U 1 1 5BFF90ED
P 3770 9510
F 0 "C4" H 3880 9440 50  0000 L CNN
F 1 "100uF" H 3870 9600 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_6.3x7.7" H 3808 9360 50  0001 C CNN
F 3 "~" H 3770 9510 50  0001 C CNN
	1    3770 9510
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C1
U 1 1 5BFF9D78
P 2870 8270
F 0 "C1" H 2960 8190 50  0000 L CNN
F 1 "100uF" H 2960 8360 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_6.3x7.7" H 2908 8120 50  0001 C CNN
F 3 "~" H 2870 8270 50  0001 C CNN
	1    2870 8270
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C6
U 1 1 5BFF9E44
P 4070 8270
F 0 "C6" H 4140 8170 50  0000 L CNN
F 1 "100uF" H 4120 8380 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_6.3x7.7" H 4108 8120 50  0001 C CNN
F 3 "~" H 4070 8270 50  0001 C CNN
	1    4070 8270
	1    0    0    -1  
$EndComp
$Comp
L arduino_shieldsNCL:ARDUINO_MEGA_SHIELD SHIELD1
U 1 1 5BD242EE
P 14010 3700
F 0 "SHIELD1" H 13960 6337 60  0000 C CNN
F 1 "ARDUINO_MEGA_SHIELD" H 13960 6231 60  0000 C CNN
F 2 "footprint:ARDUINO MEGA SHIELD" H 14010 3700 50  0001 C CNN
F 3 "" H 14010 3700 50  0001 C CNN
	1    14010 3700
	1    0    0    -1  
$EndComp
Text Label 12780 2450 0    50   ~ 0
xstep
Wire Wire Line
	13010 2450 12780 2450
Wire Wire Line
	13010 2550 12780 2550
Wire Wire Line
	13010 2650 12780 2650
Wire Wire Line
	13010 3050 12780 3050
Text Label 12780 3050 0    50   ~ 0
ystep
Wire Wire Line
	13010 3150 12780 3150
Wire Wire Line
	13010 3350 12780 3350
Text Label 12780 3350 0    50   ~ 0
zen
Text Label 12780 2550 0    50   ~ 0
xdir
Text Label 12780 2650 0    50   ~ 0
yen
Text Label 12780 3150 0    50   ~ 0
ydir
Text Label 12310 3850 0    60   ~ 0
thermometer1
Wire Wire Line
	13010 3850 12310 3850
Wire Wire Line
	13010 3950 12310 3950
Wire Wire Line
	13010 4050 12310 4050
Text Label 12310 3950 0    60   ~ 0
thermometer2
Text Label 12310 4050 0    60   ~ 0
thermometer3
Text Label 12960 1950 2    60   ~ 0
5V_from_arduino
Wire Wire Line
	13010 2050 12910 2050
Wire Wire Line
	12910 2050 12910 2150
Wire Wire Line
	12910 2150 13010 2150
Connection ~ 12910 2050
Wire Wire Line
	12910 2050 12680 2050
Wire Wire Line
	13010 2250 12680 2250
Text Label 12680 2250 0    50   ~ 0
12V-Dv
Wire Wire Line
	13010 1950 12960 1950
Text Label 12680 2050 0    50   ~ 0
GND
Wire Wire Line
	15230 1950 14910 1950
Text Label 15230 1950 2    50   ~ 0
heater1
Text Label 15230 2050 2    50   ~ 0
fan1
Wire Wire Line
	15230 2050 14910 2050
Text Label 15230 2650 2    60   ~ 0
xmin
Wire Wire Line
	15230 2650 14910 2650
Text Label 15230 2750 2    60   ~ 0
xmax
Wire Wire Line
	15230 2750 14910 2750
Wire Wire Line
	14910 4100 14960 4100
Wire Wire Line
	14960 4100 14960 4200
Wire Wire Line
	14960 4200 14910 4200
Wire Wire Line
	13010 4300 12950 4300
Wire Wire Line
	12950 4300 12950 4400
Wire Wire Line
	12950 4400 13010 4400
Text Label 12810 5300 0    50   ~ 0
zstep
Wire Wire Line
	12810 5300 13010 5300
Text Label 12810 5100 0    50   ~ 0
zdir
Wire Wire Line
	12810 5100 13010 5100
Text Label 12830 6150 0    50   ~ 0
xen
Wire Wire Line
	12830 6150 13010 6150
Text Label 15140 4950 2    50   ~ 0
edir
Wire Wire Line
	15140 4950 14910 4950
Text Label 15150 4750 2    50   ~ 0
estep
Wire Wire Line
	15150 4750 14910 4750
Text Label 15150 4550 2    50   ~ 0
een
Wire Wire Line
	15150 4550 14910 4550
Wire Wire Line
	14910 3150 15190 3150
Text Label 15190 3150 2    60   ~ 0
ymin
Wire Wire Line
	14910 3250 15190 3250
Text Label 15190 3250 2    60   ~ 0
ymax
Wire Wire Line
	14910 3550 15190 3550
Text Label 15190 3550 2    60   ~ 0
zmin
Wire Wire Line
	14910 3650 15190 3650
Text Label 15190 3650 2    60   ~ 0
zmax
$Comp
L Device:CP C14
U 1 1 5BB79645
P 8240 4530
F 0 "C14" H 8358 4576 50  0000 L CNN
F 1 "10uF" H 8358 4485 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_4x5.4" H 8278 4380 50  0001 C CNN
F 3 "2611379" H 8240 4530 50  0001 C CNN
	1    8240 4530
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C16
U 1 1 5BB79951
P 10120 4540
F 0 "C16" H 10238 4586 50  0000 L CNN
F 1 "10uF" H 10238 4495 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_4x5.4" H 10158 4390 50  0001 C CNN
F 3 "~" H 10120 4540 50  0001 C CNN
	1    10120 4540
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C13
U 1 1 5BB79D7C
P 8230 5320
F 0 "C13" H 8348 5366 50  0000 L CNN
F 1 "10uF" H 8348 5275 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_4x5.4" H 8268 5170 50  0001 C CNN
F 3 "~" H 8230 5320 50  0001 C CNN
	1    8230 5320
	1    0    0    -1  
$EndComp
Wire Wire Line
	3370 9270 3520 9270
Connection ~ 3770 9270
Wire Wire Line
	3770 9270 4120 9270
Text Notes 3520 9080 0    50   ~ 0
áram?
Text Label 4120 9270 2    50   ~ 0
12V-Dv
$Comp
L Device:Jumper JP1
U 1 1 5BD6871D
P 800 2040
F 0 "JP1" H 1010 1740 50  0000 C CNN
F 1 "Jumper" H 790 1750 50  0000 C CNN
F 2 "footprint:jumper_mode_select" H 800 2040 50  0001 C CNN
F 3 "~" H 800 2040 50  0001 C CNN
	1    800  2040
	1    0    0    -1  
$EndComp
$Comp
L Device:Jumper JP2
U 1 1 5BD68A67
P 800 2140
F 0 "JP2" H 1010 1860 50  0000 C CNN
F 1 "Jumper" H 790 1870 50  0000 C CNN
F 2 "footprint:jumper_mode_select" H 800 2140 50  0001 C CNN
F 3 "~" H 800 2140 50  0001 C CNN
	1    800  2140
	1    0    0    -1  
$EndComp
$Comp
L Device:Jumper JP3
U 1 1 5BD68B0D
P 800 2240
F 0 "JP3" H 1010 1990 50  0000 C CNN
F 1 "Jumper" H 790 2000 50  0000 C CNN
F 2 "footprint:jumper_mode_select" H 800 2240 50  0001 C CNN
F 3 "~" H 800 2240 50  0001 C CNN
	1    800  2240
	1    0    0    -1  
$EndComp
Wire Wire Line
	1100 2040 1150 2040
Wire Wire Line
	1100 2140 1250 2140
Wire Wire Line
	1100 2240 1340 2240
Text Label 500  1950 0    50   ~ 0
5V
Wire Wire Line
	500  2040 500  2140
Connection ~ 1800 2540
$Comp
L Device:Jumper JP4
U 1 1 5BE46C7B
P 3140 2130
F 0 "JP4" H 3140 2420 50  0000 C CNN
F 1 "Jumper" H 2590 2130 50  0000 C CNN
F 2 "footprint:jumper_mode_select" H 3140 2130 50  0001 C CNN
F 3 "~" H 3140 2130 50  0001 C CNN
	1    3140 2130
	1    0    0    -1  
$EndComp
$Comp
L Device:Jumper JP5
U 1 1 5BE46C81
P 3140 2230
F 0 "JP5" H 3140 2560 50  0000 C CNN
F 1 "Jumper" H 2590 2230 50  0000 C CNN
F 2 "footprint:jumper_mode_select" H 3140 2230 50  0001 C CNN
F 3 "~" H 3140 2230 50  0001 C CNN
	1    3140 2230
	1    0    0    -1  
$EndComp
$Comp
L Device:Jumper JP6
U 1 1 5BE46C87
P 3140 2330
F 0 "JP6" H 3140 2700 50  0000 C CNN
F 1 "Jumper" H 2590 2330 50  0000 C CNN
F 2 "footprint:jumper_mode_select" H 3140 2330 50  0001 C CNN
F 3 "~" H 3140 2330 50  0001 C CNN
	1    3140 2330
	1    0    0    -1  
$EndComp
Wire Wire Line
	3440 2130 3450 2130
Wire Wire Line
	3440 2230 3550 2230
Wire Wire Line
	3440 2330 3640 2330
Text Label 2750 2050 2    50   ~ 0
5V
Wire Wire Line
	2750 2050 2750 2130
Wire Wire Line
	2750 2130 2840 2130
Wire Wire Line
	2840 2130 2840 2230
Connection ~ 2840 2130
Wire Wire Line
	2840 2230 2840 2330
Connection ~ 2840 2230
$Comp
L Device:Jumper JP7
U 1 1 5BF03C77
P 5400 2030
F 0 "JP7" H 5620 1740 50  0000 C CNN
F 1 "Jumper" H 5400 1740 50  0000 C CNN
F 2 "footprint:jumper_mode_select" H 5400 2030 50  0001 C CNN
F 3 "~" H 5400 2030 50  0001 C CNN
	1    5400 2030
	1    0    0    -1  
$EndComp
$Comp
L Device:Jumper JP8
U 1 1 5BF03C7D
P 5400 2130
F 0 "JP8" H 5620 1870 50  0000 C CNN
F 1 "Jumper" H 5400 1870 50  0000 C CNN
F 2 "footprint:jumper_mode_select" H 5400 2130 50  0001 C CNN
F 3 "~" H 5400 2130 50  0001 C CNN
	1    5400 2130
	1    0    0    -1  
$EndComp
$Comp
L Device:Jumper JP9
U 1 1 5BF03C83
P 5400 2230
F 0 "JP9" H 5620 2000 50  0000 C CNN
F 1 "Jumper" H 5400 2000 50  0000 C CNN
F 2 "footprint:jumper_mode_select" H 5400 2230 50  0001 C CNN
F 3 "~" H 5400 2230 50  0001 C CNN
	1    5400 2230
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 2030 5750 2030
Wire Wire Line
	5700 2130 5850 2130
Wire Wire Line
	5700 2230 5940 2230
Text Label 5100 1910 0    50   ~ 0
5V
Wire Wire Line
	5100 2030 5100 2130
Wire Wire Line
	5100 2130 5100 2230
Connection ~ 5100 2130
$Comp
L Device:Jumper JP13
U 1 1 5BF36E73
P 9960 2120
F 0 "JP13" H 10160 1810 50  0000 C CNN
F 1 "Jumper" H 9930 1810 50  0000 C CNN
F 2 "footprint:jumper_mode_select" H 9960 2120 50  0001 C CNN
F 3 "~" H 9960 2120 50  0001 C CNN
	1    9960 2120
	1    0    0    -1  
$EndComp
$Comp
L Device:Jumper JP14
U 1 1 5BF36E79
P 9960 2220
F 0 "JP14" H 10160 1930 50  0000 C CNN
F 1 "Jumper" H 9930 1930 50  0000 C CNN
F 2 "footprint:jumper_mode_select" H 9960 2220 50  0001 C CNN
F 3 "~" H 9960 2220 50  0001 C CNN
	1    9960 2220
	1    0    0    -1  
$EndComp
$Comp
L Device:Jumper JP15
U 1 1 5BF36E7F
P 9960 2320
F 0 "JP15" H 10160 2050 50  0000 C CNN
F 1 "Jumper" H 9930 2050 50  0000 C CNN
F 2 "footprint:jumper_mode_select" H 9960 2320 50  0001 C CNN
F 3 "~" H 9960 2320 50  0001 C CNN
	1    9960 2320
	1    0    0    -1  
$EndComp
Wire Wire Line
	10260 2120 10290 2120
Wire Wire Line
	10260 2220 10390 2220
Wire Wire Line
	10260 2320 10480 2320
Text Label 9660 1990 0    50   ~ 0
5V
Wire Wire Line
	9660 2120 9660 2220
Wire Wire Line
	9660 2220 9660 2320
Connection ~ 9660 2220
$Comp
L Device:Jumper JP10
U 1 1 5BF84926
P 7780 2070
F 0 "JP10" H 8000 1770 50  0000 C CNN
F 1 "Jumper" H 7760 1770 50  0000 C CNN
F 2 "footprint:jumper_mode_select" H 7780 2070 50  0001 C CNN
F 3 "~" H 7780 2070 50  0001 C CNN
	1    7780 2070
	1    0    0    -1  
$EndComp
$Comp
L Device:Jumper JP11
U 1 1 5BF8492C
P 7780 2170
F 0 "JP11" H 8000 1890 50  0000 C CNN
F 1 "Jumper" H 7760 1890 50  0000 C CNN
F 2 "footprint:jumper_mode_select" H 7780 2170 50  0001 C CNN
F 3 "~" H 7780 2170 50  0001 C CNN
	1    7780 2170
	1    0    0    -1  
$EndComp
$Comp
L Device:Jumper JP12
U 1 1 5BF84932
P 7780 2270
F 0 "JP12" H 8000 2010 50  0000 C CNN
F 1 "Jumper" H 7760 2010 50  0000 C CNN
F 2 "footprint:jumper_mode_select" H 7780 2270 50  0001 C CNN
F 3 "~" H 7780 2270 50  0001 C CNN
	1    7780 2270
	1    0    0    -1  
$EndComp
Wire Wire Line
	8080 2070 8120 2070
Wire Wire Line
	8080 2170 8220 2170
Wire Wire Line
	8080 2270 8310 2270
Text Label 7480 1950 0    50   ~ 0
5V
Wire Wire Line
	7480 2070 7480 2170
Wire Wire Line
	7480 2170 7480 2270
Connection ~ 7480 2170
$Comp
L logo:logo LOGO1
U 1 1 5BBCECC0
P 15500 690
F 0 "LOGO1" H 15525 736 50  0000 L CNN
F 1 "logo" H 15525 645 50  0000 L CNN
F 2 "logo:LOGO" H 15500 690 50  0001 C CNN
F 3 "" H 15500 690 50  0001 C CNN
	1    15500 690 
	1    0    0    -1  
$EndComp
$Comp
L Driver_Motor:Pololu_Breakout_DRV8825 A3
U 1 1 5BAAA451
P 6350 1630
F 0 "A3" H 6150 830 50  0000 C CNN
F 1 "Pololu_Breakout_DRV8825" H 6350 2480 50  0000 C CNN
F 2 "Module:Pololu_Breakout-16_15.2x20.3mm" H 6550 830 50  0001 L CNN
F 3 "https://www.pololu.com/product/2982" H 6450 1330 50  0001 C CNN
	1    6350 1630
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J15
U 1 1 5BCA9059
P 9520 1720
F 0 "J15" H 9450 1420 50  0000 L CNN
F 1 "Conn_01x04" H 9290 1940 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x04_P2.54mm_Vertical" H 9520 1720 50  0001 C CNN
F 3 "~" H 9520 1720 50  0001 C CNN
	1    9520 1720
	1    0    0    -1  
$EndComp
Wire Wire Line
	9120 1670 9320 1670
Wire Wire Line
	9320 1670 9320 1620
Wire Wire Line
	9320 1720 9140 1720
Wire Wire Line
	9140 1720 9140 1570
Wire Wire Line
	9140 1570 9120 1570
Wire Wire Line
	6750 1530 6780 1530
Wire Wire Line
	6780 1530 6780 1680
Wire Wire Line
	6780 1680 6950 1680
Wire Wire Line
	6750 1630 6950 1630
Wire Wire Line
	6950 1630 6950 1580
Wire Wire Line
	11290 1720 11490 1720
Wire Wire Line
	11490 1720 11490 1670
Wire Wire Line
	11490 1770 11310 1770
Wire Wire Line
	11310 1770 11310 1620
Wire Wire Line
	11310 1620 11290 1620
Wire Wire Line
	4440 1730 4640 1730
Wire Wire Line
	4640 1730 4640 1680
Wire Wire Line
	4640 1780 4460 1780
Wire Wire Line
	4460 1780 4460 1630
Wire Wire Line
	4460 1630 4440 1630
Wire Wire Line
	2350 1690 2170 1690
Wire Wire Line
	2170 1690 2170 1540
Wire Wire Line
	2170 1540 2150 1540
Wire Wire Line
	2150 1640 2350 1640
Wire Wire Line
	2350 1640 2350 1590
Wire Wire Line
	3450 2130 3450 2380
Connection ~ 3450 2130
Wire Wire Line
	3450 2130 3640 2130
Wire Wire Line
	3550 2230 3550 2380
Connection ~ 3550 2230
Wire Wire Line
	3550 2230 3640 2230
Wire Wire Line
	3640 2330 3640 2380
Connection ~ 3640 2330
$Comp
L Device:Jumper_NC_Small JP23
U 1 1 5C07B4BC
P 3450 2480
F 0 "JP23" H 3160 2480 50  0000 R CNN
F 1 "Jumper" H 3500 2830 50  0000 R CNN
F 2 "footprint:jumper_mode_select" H 3450 2480 50  0001 C CNN
F 3 "~" H 3450 2480 50  0001 C CNN
	1    3450 2480
	0    -1   -1   0   
$EndComp
$Comp
L Device:Jumper_NC_Small JP25
U 1 1 5C07B76A
P 3550 2480
F 0 "JP25" H 3260 2480 50  0000 R CNN
F 1 "Jumper" H 3600 2850 50  0000 R CNN
F 2 "footprint:jumper_mode_select" H 3550 2480 50  0001 C CNN
F 3 "~" H 3550 2480 50  0001 C CNN
	1    3550 2480
	0    -1   -1   0   
$EndComp
$Comp
L Device:Jumper_NC_Small JP27
U 1 1 5C07B840
P 3640 2480
F 0 "JP27" H 3350 2480 50  0000 R CNN
F 1 "Jumper" H 3690 2860 50  0000 R CNN
F 2 "footprint:jumper_mode_select" H 3640 2480 50  0001 C CNN
F 3 "~" H 3640 2480 50  0001 C CNN
	1    3640 2480
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3450 2580 3450 2730
Text Label 3450 2730 1    50   ~ 0
SDI
Wire Wire Line
	3550 2580 3550 2730
Text Label 3550 2730 1    50   ~ 0
SCK
Wire Wire Line
	3640 2580 3640 2730
Text Label 3640 2730 1    50   ~ 0
CS_Y
Wire Wire Line
	3640 1330 3640 1280
$Comp
L Device:Jumper_NC_Small JP26
U 1 1 5C11E17E
P 3640 1180
F 0 "JP26" V 3720 1160 50  0000 R CNN
F 1 "Jumper" V 3650 1160 50  0000 R CNN
F 2 "footprint:jumper_mode_select" H 3640 1180 50  0001 C CNN
F 3 "~" H 3640 1180 50  0001 C CNN
	1    3640 1180
	0    -1   -1   0   
$EndComp
Text Label 3640 1020 0    50   ~ 0
5V
Wire Wire Line
	3640 1020 3640 1080
$Comp
L Device:Jumper_NC_Small JP24
U 1 1 5C15FFA9
P 3470 1360
F 0 "JP24" H 3560 1470 50  0000 R CNN
F 1 "Jumper" H 3610 1540 50  0000 R CNN
F 2 "footprint:jumper_mode_select" H 3470 1360 50  0001 C CNN
F 3 "~" H 3470 1360 50  0001 C CNN
	1    3470 1360
	-1   0    0    1   
$EndComp
Wire Wire Line
	3570 1360 3640 1360
Wire Wire Line
	3640 1360 3640 1430
Connection ~ 3640 1430
Text Label 3270 1360 0    50   ~ 0
SDO
Wire Wire Line
	590  1640 760  1640
$Comp
L Device:R R12
U 1 1 5C34EDD7
P 760 1490
F 0 "R12" V 840 1410 50  0000 L CNN
F 1 "10k" V 680 1420 50  0000 L CNN
F 2 "Resistors_SMD:R_0603" V 690 1490 50  0001 C CNN
F 3 "~" H 760 1490 50  0001 C CNN
	1    760  1490
	1    0    0    -1  
$EndComp
$Comp
L Device:Jumper_NC_Small JP17
U 1 1 5C34EDDD
P 830 1240
F 0 "JP17" H 490 1190 50  0000 L CNN
F 1 "Jumper" H 630 1180 50  0000 L CNN
F 2 "footprint:jumper_mode_select" H 830 1240 50  0001 C CNN
F 3 "~" H 830 1240 50  0001 C CNN
	1    830  1240
	0    1    1    0   
$EndComp
$Comp
L Device:Jumper_NC_Small JP16
U 1 1 5C34EDE3
P 700 1240
F 0 "JP16" H 1040 1380 50  0000 R CNN
F 1 "Jumper" H 900 1390 50  0000 R CNN
F 2 "footprint:jumper_mode_select" H 700 1240 50  0001 C CNN
F 3 "~" H 700 1240 50  0001 C CNN
	1    700  1240
	0    -1   -1   0   
$EndComp
Wire Wire Line
	700  1140 700  1070
Wire Wire Line
	830  1140 830  1070
Text Label 830  1070 0    50   ~ 0
GND
Text Label 700  1070 2    50   ~ 0
5V
Wire Wire Line
	700  1340 760  1340
Connection ~ 760  1340
Wire Wire Line
	760  1340 830  1340
Wire Wire Line
	5950 1330 5950 1430
$Comp
L Device:Jumper_NC_Small JP30
U 1 1 5C45CC11
P 5750 2480
F 0 "JP30" H 5460 2480 50  0000 R CNN
F 1 "Jumper" H 5310 2480 50  0000 R CNN
F 2 "footprint:jumper_mode_select" H 5750 2480 50  0001 C CNN
F 3 "~" H 5750 2480 50  0001 C CNN
	1    5750 2480
	0    -1   -1   0   
$EndComp
$Comp
L Device:Jumper_NC_Small JP31
U 1 1 5C45CC17
P 5850 2480
F 0 "JP31" H 5560 2480 50  0000 R CNN
F 1 "Jumper" H 5410 2480 50  0000 R CNN
F 2 "footprint:jumper_mode_select" H 5850 2480 50  0001 C CNN
F 3 "~" H 5850 2480 50  0001 C CNN
	1    5850 2480
	0    -1   -1   0   
$EndComp
$Comp
L Device:Jumper_NC_Small JP32
U 1 1 5C45CC1D
P 5940 2480
F 0 "JP32" H 5650 2480 50  0000 R CNN
F 1 "Jumper" H 5500 2480 50  0000 R CNN
F 2 "footprint:jumper_mode_select" H 5940 2480 50  0001 C CNN
F 3 "~" H 5940 2480 50  0001 C CNN
	1    5940 2480
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5750 2580 5750 2770
Text Label 5750 2770 1    50   ~ 0
SDI
Wire Wire Line
	5850 2580 5850 2770
Text Label 5850 2770 1    50   ~ 0
SCK
Wire Wire Line
	5940 2580 5940 2770
Text Label 5940 2770 1    50   ~ 0
CS_Z1
$Comp
L Device:Jumper_NC_Small JP35
U 1 1 5C47F878
P 8120 2480
F 0 "JP35" H 7800 2480 50  0000 R CNN
F 1 "Jumper" H 7670 2480 50  0000 R CNN
F 2 "footprint:jumper_mode_select" H 8120 2480 50  0001 C CNN
F 3 "~" H 8120 2480 50  0001 C CNN
	1    8120 2480
	0    -1   -1   0   
$EndComp
$Comp
L Device:Jumper_NC_Small JP36
U 1 1 5C47F87E
P 8220 2480
F 0 "JP36" H 7900 2480 50  0000 R CNN
F 1 "Jumper" H 7770 2480 50  0000 R CNN
F 2 "footprint:jumper_mode_select" H 8220 2480 50  0001 C CNN
F 3 "~" H 8220 2480 50  0001 C CNN
	1    8220 2480
	0    -1   -1   0   
$EndComp
$Comp
L Device:Jumper_NC_Small JP37
U 1 1 5C47F884
P 8310 2480
F 0 "JP37" H 7990 2480 50  0000 R CNN
F 1 "Jumper" H 7860 2480 50  0000 R CNN
F 2 "footprint:jumper_mode_select" H 8310 2480 50  0001 C CNN
F 3 "~" H 8310 2480 50  0001 C CNN
	1    8310 2480
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8120 2580 8120 2780
Text Label 8120 2780 1    50   ~ 0
SDI
Wire Wire Line
	8220 2580 8220 2780
Text Label 8220 2780 1    50   ~ 0
SCK
Wire Wire Line
	8310 2580 8310 2780
Text Label 8310 2780 1    50   ~ 0
CS_Z2
$Comp
L Device:Jumper_NC_Small JP40
U 1 1 5C4A38D0
P 10290 2480
F 0 "JP40" H 9990 2480 50  0000 R CNN
F 1 "Jumper" H 9850 2480 50  0000 R CNN
F 2 "footprint:jumper_mode_select" H 10290 2480 50  0001 C CNN
F 3 "~" H 10290 2480 50  0001 C CNN
	1    10290 2480
	0    -1   -1   0   
$EndComp
$Comp
L Device:Jumper_NC_Small JP41
U 1 1 5C4A38D6
P 10390 2480
F 0 "JP41" H 10090 2480 50  0000 R CNN
F 1 "Jumper" H 9950 2480 50  0000 R CNN
F 2 "footprint:jumper_mode_select" H 10390 2480 50  0001 C CNN
F 3 "~" H 10390 2480 50  0001 C CNN
	1    10390 2480
	0    -1   -1   0   
$EndComp
$Comp
L Device:Jumper_NC_Small JP42
U 1 1 5C4A38DC
P 10480 2480
F 0 "JP42" H 10180 2480 50  0000 R CNN
F 1 "Jumper" H 10040 2480 50  0000 R CNN
F 2 "footprint:jumper_mode_select" H 10480 2480 50  0001 C CNN
F 3 "~" H 10480 2480 50  0001 C CNN
	1    10480 2480
	0    -1   -1   0   
$EndComp
Wire Wire Line
	10290 2580 10290 2760
Text Label 10290 2760 1    50   ~ 0
SDI
Wire Wire Line
	10390 2580 10390 2760
Text Label 10390 2760 1    50   ~ 0
SCK
Wire Wire Line
	10480 2580 10480 2760
Text Label 10480 2760 1    50   ~ 0
CS_E
Connection ~ 500  2140
Wire Wire Line
	500  2140 500  2240
$Comp
L Device:Jumper_NC_Small JP18
U 1 1 5C5581B4
P 1150 2480
F 0 "JP18" H 860 2480 50  0000 R CNN
F 1 "Jumper" H 680 2480 50  0000 R CNN
F 2 "footprint:jumper_mode_select" H 1150 2480 50  0001 C CNN
F 3 "~" H 1150 2480 50  0001 C CNN
	1    1150 2480
	0    -1   -1   0   
$EndComp
$Comp
L Device:Jumper_NC_Small JP19
U 1 1 5C5581BA
P 1250 2480
F 0 "JP19" H 960 2480 50  0000 R CNN
F 1 "Jumper" H 780 2480 50  0000 R CNN
F 2 "footprint:jumper_mode_select" H 1250 2480 50  0001 C CNN
F 3 "~" H 1250 2480 50  0001 C CNN
	1    1250 2480
	0    -1   -1   0   
$EndComp
$Comp
L Device:Jumper_NC_Small JP20
U 1 1 5C5581C0
P 1340 2480
F 0 "JP20" H 1050 2480 50  0000 R CNN
F 1 "Jumper" H 870 2480 50  0000 R CNN
F 2 "footprint:jumper_mode_select" H 1340 2480 50  0001 C CNN
F 3 "~" H 1340 2480 50  0001 C CNN
	1    1340 2480
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1150 2580 1150 2730
Text Label 1150 2730 1    50   ~ 0
SDI
Wire Wire Line
	1250 2580 1250 2730
Text Label 1250 2730 1    50   ~ 0
SCK
Wire Wire Line
	1340 2580 1340 2730
Text Label 1340 2730 1    50   ~ 0
CS_X
Wire Wire Line
	500  1950 500  2040
Wire Wire Line
	500  2040 490  2040
Connection ~ 500  2040
Wire Wire Line
	1150 2380 1150 2040
Connection ~ 1150 2040
Wire Wire Line
	1150 2040 1350 2040
Wire Wire Line
	1250 2380 1250 2140
Connection ~ 1250 2140
Wire Wire Line
	1250 2140 1350 2140
Wire Wire Line
	1340 2380 1340 2240
Connection ~ 1340 2240
Wire Wire Line
	1340 2240 1350 2240
Wire Wire Line
	5100 1910 5100 2030
Connection ~ 5100 2030
Wire Wire Line
	5750 2380 5750 2030
Connection ~ 5750 2030
Wire Wire Line
	5750 2030 5950 2030
Wire Wire Line
	5850 2380 5850 2130
Connection ~ 5850 2130
Wire Wire Line
	5850 2130 5950 2130
Wire Wire Line
	5940 2380 5940 2230
Connection ~ 5940 2230
Wire Wire Line
	5940 2230 5950 2230
Wire Wire Line
	7480 1950 7480 2070
Connection ~ 7480 2070
Wire Wire Line
	8120 2380 8120 2070
Connection ~ 8120 2070
Wire Wire Line
	8120 2070 8320 2070
Wire Wire Line
	8220 2380 8220 2170
Connection ~ 8220 2170
Wire Wire Line
	8220 2170 8320 2170
Wire Wire Line
	8310 2380 8310 2270
Connection ~ 8310 2270
Wire Wire Line
	8310 2270 8320 2270
Wire Wire Line
	9660 1990 9660 2120
Connection ~ 9660 2120
Wire Wire Line
	10290 2380 10290 2120
Connection ~ 10290 2120
Wire Wire Line
	10290 2120 10490 2120
Wire Wire Line
	10390 2380 10390 2220
Connection ~ 10390 2220
Wire Wire Line
	10390 2220 10490 2220
Wire Wire Line
	10480 2380 10480 2320
Connection ~ 10480 2320
Wire Wire Line
	10480 2320 10490 2320
Wire Wire Line
	13010 1750 12930 1750
$Comp
L Switch:SW_Push SW1
U 1 1 5CDD8159
P 12730 1750
F 0 "SW1" H 12730 2035 50  0000 C CNN
F 1 "SW_Push" H 12730 1944 50  0000 C CNN
F 2 "Buttons_Switches_SMD:SW_SPST_PTS645" H 12730 1950 50  0001 C CNN
F 3 "" H 12730 1950 50  0001 C CNN
	1    12730 1750
	1    0    0    -1  
$EndComp
Wire Wire Line
	12530 1750 12350 1750
Text Label 12350 1750 0    50   ~ 0
GND
Wire Notes Line
	2410 3470 6290 3470
Wire Notes Line
	6290 3470 6290 5300
Wire Notes Line
	6290 5300 2410 5300
Wire Notes Line
	2410 5300 2410 3470
Wire Notes Line
	4130 5340 4130 6950
Wire Notes Line
	4130 6950 2000 6950
Wire Notes Line
	2000 6950 2000 5340
Wire Notes Line
	2000 5340 4130 5340
Wire Notes Line
	6290 5350 6290 7360
Wire Notes Line
	6290 7360 4210 7360
Wire Notes Line
	4210 7360 4210 5350
Wire Notes Line
	4210 5350 6290 5350
Wire Notes Line
	6610 3450 11420 3450
Wire Notes Line
	11420 3450 11420 5840
Wire Notes Line
	11420 5840 6610 5840
Wire Notes Line
	6610 5840 6610 3450
Wire Notes Line
	15560 6940 12110 6940
Wire Wire Line
	7600 1670 7770 1670
Wire Wire Line
	9820 1720 10000 1720
Wire Wire Line
	10490 1420 10490 1520
Wire Wire Line
	8320 1370 8320 1470
Wire Wire Line
	5180 1630 5360 1630
Wire Wire Line
	2900 1730 3070 1730
Connection ~ 760  1640
Wire Wire Line
	760  1640 1350 1640
$Comp
L Device:R R13
U 1 1 5BE4E6C9
P 3070 1580
F 0 "R13" V 3150 1500 50  0000 L CNN
F 1 "10k" V 2990 1510 50  0000 L CNN
F 2 "Resistors_SMD:R_0603" V 3000 1580 50  0001 C CNN
F 3 "~" H 3070 1580 50  0001 C CNN
	1    3070 1580
	1    0    0    -1  
$EndComp
$Comp
L Device:Jumper_NC_Small JP29
U 1 1 5BE4E6CF
P 3140 1330
F 0 "JP29" H 2800 1280 50  0000 L CNN
F 1 "Jumper" H 2940 1270 50  0000 L CNN
F 2 "footprint:jumper_mode_select" H 3140 1330 50  0001 C CNN
F 3 "~" H 3140 1330 50  0001 C CNN
	1    3140 1330
	0    1    1    0   
$EndComp
$Comp
L Device:Jumper_NC_Small JP28
U 1 1 5BE4E6D5
P 3010 1330
F 0 "JP28" H 3350 1470 50  0000 R CNN
F 1 "Jumper" H 3210 1480 50  0000 R CNN
F 2 "footprint:jumper_mode_select" H 3010 1330 50  0001 C CNN
F 3 "~" H 3010 1330 50  0001 C CNN
	1    3010 1330
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3010 1230 3010 1160
Wire Wire Line
	3140 1230 3140 1160
Text Label 3140 1160 0    50   ~ 0
GND
Text Label 3010 1160 2    50   ~ 0
5V
Wire Wire Line
	3010 1430 3070 1430
Connection ~ 3070 1430
Wire Wire Line
	3070 1430 3140 1430
Connection ~ 3070 1730
Wire Wire Line
	3070 1730 3640 1730
$Comp
L Device:R R14
U 1 1 5BE73E96
P 5360 1480
F 0 "R14" V 5440 1400 50  0000 L CNN
F 1 "10k" V 5280 1410 50  0000 L CNN
F 2 "Resistors_SMD:R_0603" V 5290 1480 50  0001 C CNN
F 3 "~" H 5360 1480 50  0001 C CNN
	1    5360 1480
	1    0    0    -1  
$EndComp
$Comp
L Device:Jumper_NC_Small JP34
U 1 1 5BE73E9C
P 5430 1230
F 0 "JP34" H 5090 1180 50  0000 L CNN
F 1 "Jumper" H 5230 1170 50  0000 L CNN
F 2 "footprint:jumper_mode_select" H 5430 1230 50  0001 C CNN
F 3 "~" H 5430 1230 50  0001 C CNN
	1    5430 1230
	0    1    1    0   
$EndComp
$Comp
L Device:Jumper_NC_Small JP33
U 1 1 5BE73EA2
P 5300 1230
F 0 "JP33" H 5640 1370 50  0000 R CNN
F 1 "Jumper" H 5500 1380 50  0000 R CNN
F 2 "footprint:jumper_mode_select" H 5300 1230 50  0001 C CNN
F 3 "~" H 5300 1230 50  0001 C CNN
	1    5300 1230
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5300 1130 5300 1060
Wire Wire Line
	5430 1130 5430 1060
Text Label 5430 1060 0    50   ~ 0
GND
Text Label 5300 1060 2    50   ~ 0
5V
Wire Wire Line
	5300 1330 5360 1330
Connection ~ 5360 1330
Wire Wire Line
	5360 1330 5430 1330
Connection ~ 5360 1630
Wire Wire Line
	5360 1630 5950 1630
$Comp
L Device:R R15
U 1 1 5BE9AC76
P 7770 1520
F 0 "R15" V 7850 1440 50  0000 L CNN
F 1 "10k" V 7690 1450 50  0000 L CNN
F 2 "Resistors_SMD:R_0603" V 7700 1520 50  0001 C CNN
F 3 "~" H 7770 1520 50  0001 C CNN
	1    7770 1520
	1    0    0    -1  
$EndComp
$Comp
L Device:Jumper_NC_Small JP44
U 1 1 5BE9AC7C
P 7840 1270
F 0 "JP44" H 7500 1220 50  0000 L CNN
F 1 "Jumper" H 7640 1210 50  0000 L CNN
F 2 "footprint:jumper_mode_select" H 7840 1270 50  0001 C CNN
F 3 "~" H 7840 1270 50  0001 C CNN
	1    7840 1270
	0    1    1    0   
$EndComp
$Comp
L Device:Jumper_NC_Small JP43
U 1 1 5BE9AC82
P 7710 1270
F 0 "JP43" H 8050 1410 50  0000 R CNN
F 1 "Jumper" H 7910 1420 50  0000 R CNN
F 2 "footprint:jumper_mode_select" H 7710 1270 50  0001 C CNN
F 3 "~" H 7710 1270 50  0001 C CNN
	1    7710 1270
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7710 1170 7710 1100
Wire Wire Line
	7840 1170 7840 1100
Text Label 7840 1100 0    50   ~ 0
GND
Text Label 7710 1100 2    50   ~ 0
5V
Wire Wire Line
	7710 1370 7770 1370
Connection ~ 7770 1370
Wire Wire Line
	7770 1370 7840 1370
Connection ~ 7770 1670
Wire Wire Line
	7770 1670 8320 1670
$Comp
L Device:R R16
U 1 1 5BEC1A81
P 10000 1570
F 0 "R16" V 10080 1490 50  0000 L CNN
F 1 "10k" V 9920 1500 50  0000 L CNN
F 2 "Resistors_SMD:R_0603" V 9930 1570 50  0001 C CNN
F 3 "~" H 10000 1570 50  0001 C CNN
	1    10000 1570
	1    0    0    -1  
$EndComp
$Comp
L Device:Jumper_NC_Small JP48
U 1 1 5BEC1A87
P 10070 1320
F 0 "JP48" H 9730 1270 50  0000 L CNN
F 1 "Jumper" H 9870 1260 50  0000 L CNN
F 2 "footprint:jumper_mode_select" H 10070 1320 50  0001 C CNN
F 3 "~" H 10070 1320 50  0001 C CNN
	1    10070 1320
	0    1    1    0   
$EndComp
$Comp
L Device:Jumper_NC_Small JP47
U 1 1 5BEC1A8D
P 9940 1320
F 0 "JP47" H 10280 1460 50  0000 R CNN
F 1 "Jumper" H 10140 1470 50  0000 R CNN
F 2 "footprint:jumper_mode_select" H 9940 1320 50  0001 C CNN
F 3 "~" H 9940 1320 50  0001 C CNN
	1    9940 1320
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9940 1220 9940 1150
Wire Wire Line
	10070 1220 10070 1150
Text Label 10070 1150 0    50   ~ 0
GND
Text Label 9940 1150 2    50   ~ 0
5V
Wire Wire Line
	9940 1420 10000 1420
Connection ~ 10000 1420
Wire Wire Line
	10000 1420 10070 1420
Connection ~ 10000 1720
Wire Wire Line
	10000 1720 10490 1720
Wire Wire Line
	3640 1430 3640 1530
Wire Wire Line
	3370 1360 3270 1360
$Comp
L Device:Jumper_NC_Small JP21
U 1 1 5C00265A
P 1180 1290
F 0 "JP21" H 1270 1400 50  0000 R CNN
F 1 "Jumper" H 1320 1470 50  0000 R CNN
F 2 "footprint:jumper_mode_select" H 1180 1290 50  0001 C CNN
F 3 "~" H 1180 1290 50  0001 C CNN
	1    1180 1290
	-1   0    0    1   
$EndComp
Text Label 980  1290 0    50   ~ 0
SDO
Wire Wire Line
	1080 1290 980  1290
Wire Wire Line
	1280 1290 1350 1290
Wire Wire Line
	1350 1290 1350 1340
Connection ~ 1350 1340
Wire Wire Line
	1350 1340 1350 1440
$Comp
L Device:Jumper_NC_Small JP22
U 1 1 5C054DC4
P 1350 1090
F 0 "JP22" V 1430 1070 50  0000 R CNN
F 1 "Jumper" V 1360 1070 50  0000 R CNN
F 2 "footprint:jumper_mode_select" H 1350 1090 50  0001 C CNN
F 3 "~" H 1350 1090 50  0001 C CNN
	1    1350 1090
	0    -1   -1   0   
$EndComp
Text Label 1350 930  0    50   ~ 0
5V
Wire Wire Line
	1350 930  1350 990 
Wire Wire Line
	1350 1190 1350 1240
$Comp
L Device:Jumper_NC_Small JP39
U 1 1 5C0A8D95
P 5950 1080
F 0 "JP39" V 6030 1060 50  0000 R CNN
F 1 "Jumper" V 5960 1060 50  0000 R CNN
F 2 "footprint:jumper_mode_select" H 5950 1080 50  0001 C CNN
F 3 "~" H 5950 1080 50  0001 C CNN
	1    5950 1080
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5950 920  5950 980 
Text Label 5950 920  0    50   ~ 0
5V
Wire Wire Line
	5950 1180 5950 1230
$Comp
L Device:Jumper_NC_Small JP38
U 1 1 5C0FD773
P 5770 1290
F 0 "JP38" H 5860 1400 50  0000 R CNN
F 1 "Jumper" H 5910 1470 50  0000 R CNN
F 2 "footprint:jumper_mode_select" H 5770 1290 50  0001 C CNN
F 3 "~" H 5770 1290 50  0001 C CNN
	1    5770 1290
	-1   0    0    1   
$EndComp
Text Label 5570 1290 0    50   ~ 0
SDO
Wire Wire Line
	5670 1290 5570 1290
Wire Wire Line
	5870 1290 5950 1290
Wire Wire Line
	5950 1290 5950 1330
Connection ~ 5950 1330
$Comp
L Device:Jumper_NC_Small JP45
U 1 1 5C152681
P 8150 1330
F 0 "JP45" H 8240 1440 50  0000 R CNN
F 1 "Jumper" H 8290 1510 50  0000 R CNN
F 2 "footprint:jumper_mode_select" H 8150 1330 50  0001 C CNN
F 3 "~" H 8150 1330 50  0001 C CNN
	1    8150 1330
	-1   0    0    1   
$EndComp
Text Label 7950 1330 0    50   ~ 0
SDO
Wire Wire Line
	8050 1330 7950 1330
Wire Wire Line
	8250 1330 8320 1330
Wire Wire Line
	8320 1330 8320 1370
Connection ~ 8320 1370
$Comp
L Device:Jumper_NC_Small JP46
U 1 1 5C1A8F79
P 8320 1120
F 0 "JP46" V 8400 1100 50  0000 R CNN
F 1 "Jumper" V 8330 1100 50  0000 R CNN
F 2 "footprint:jumper_mode_select" H 8320 1120 50  0001 C CNN
F 3 "~" H 8320 1120 50  0001 C CNN
	1    8320 1120
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8320 960  8320 1020
Wire Wire Line
	8320 1220 8320 1270
Text Label 8320 960  0    50   ~ 0
5V
$Comp
L Device:Jumper_NC_Small JP50
U 1 1 5C2006BB
P 10490 1160
F 0 "JP50" V 10570 1140 50  0000 R CNN
F 1 "Jumper" V 10500 1140 50  0000 R CNN
F 2 "footprint:jumper_mode_select" H 10490 1160 50  0001 C CNN
F 3 "~" H 10490 1160 50  0001 C CNN
	1    10490 1160
	0    -1   -1   0   
$EndComp
Wire Wire Line
	10490 1000 10490 1060
Wire Wire Line
	10490 1260 10490 1320
Text Label 10490 1000 0    50   ~ 0
5V
$Comp
L Device:Jumper_NC_Small JP49
U 1 1 5C2AEE1B
P 10350 1380
F 0 "JP49" H 10440 1490 50  0000 R CNN
F 1 "Jumper" H 10490 1560 50  0000 R CNN
F 2 "footprint:jumper_mode_select" H 10350 1380 50  0001 C CNN
F 3 "~" H 10350 1380 50  0001 C CNN
	1    10350 1380
	-1   0    0    1   
$EndComp
Text Label 10150 1380 0    50   ~ 0
SDO
Wire Wire Line
	10250 1380 10150 1380
Wire Wire Line
	10450 1380 10490 1380
Wire Wire Line
	10490 1380 10490 1420
Connection ~ 10490 1420
Wire Wire Line
	14910 1750 15230 1750
Text Label 15230 1750 2    50   ~ 0
servo1
Wire Wire Line
	14910 2450 15230 2450
Text Label 15230 2450 2    50   ~ 0
servo3
Wire Wire Line
	14910 2350 15230 2350
Text Label 15230 2350 2    50   ~ 0
servo2
Wire Wire Line
	14910 2550 15230 2550
Text Label 15230 2550 2    50   ~ 0
servo4
Text Notes 6860 6130 0    118  ~ 24
servos
Wire Wire Line
	7920 6360 8420 6360
Text Label 8420 6360 2    50   ~ 0
servo1
$Comp
L Connector_Generic:Conn_01x03 J18
U 1 1 5C49E2A3
P 7720 6460
F 0 "J18" H 7720 6660 50  0000 C CNN
F 1 "Conn_01x03" H 7640 6226 50  0000 C CNN
F 2 "footprint:PinSocket_1x03_P2.54mm_Vertical" H 7720 6460 50  0001 C CNN
F 3 "~" H 7720 6460 50  0001 C CNN
	1    7720 6460
	-1   0    0    1   
$EndComp
Wire Wire Line
	7920 6460 8420 6460
Text Label 8420 6460 2    50   ~ 0
GND
Wire Wire Line
	7920 6560 8420 6560
Text Label 8420 6560 2    50   ~ 0
servo_power
Wire Wire Line
	8810 6360 9310 6360
Text Label 9310 6360 2    50   ~ 0
servo2
$Comp
L Connector_Generic:Conn_01x03 J20
U 1 1 5C60C400
P 8610 6460
F 0 "J20" H 8610 6660 50  0000 C CNN
F 1 "Conn_01x03" H 8530 6226 50  0000 C CNN
F 2 "footprint:PinSocket_1x03_P2.54mm_Vertical" H 8610 6460 50  0001 C CNN
F 3 "~" H 8610 6460 50  0001 C CNN
	1    8610 6460
	-1   0    0    1   
$EndComp
Wire Wire Line
	8810 6460 9310 6460
Text Label 9310 6460 2    50   ~ 0
GND
Wire Wire Line
	8810 6560 9310 6560
Text Label 9310 6560 2    50   ~ 0
servo_power
Wire Wire Line
	7920 7000 8420 7000
Text Label 8420 7000 2    50   ~ 0
servo3
$Comp
L Connector_Generic:Conn_01x03 J19
U 1 1 5C63B16A
P 7720 7100
F 0 "J19" H 7720 7300 50  0000 C CNN
F 1 "Conn_01x03" H 7640 6866 50  0000 C CNN
F 2 "footprint:PinSocket_1x03_P2.54mm_Vertical" H 7720 7100 50  0001 C CNN
F 3 "~" H 7720 7100 50  0001 C CNN
	1    7720 7100
	-1   0    0    1   
$EndComp
Wire Wire Line
	7920 7100 8420 7100
Text Label 8420 7100 2    50   ~ 0
GND
Wire Wire Line
	7920 7200 8420 7200
Text Label 8420 7200 2    50   ~ 0
servo_power
Wire Wire Line
	8810 7000 9310 7000
Text Label 9310 7000 2    50   ~ 0
servo4
$Comp
L Connector_Generic:Conn_01x03 J21
U 1 1 5C63B176
P 8610 7100
F 0 "J21" H 8610 7300 50  0000 C CNN
F 1 "Conn_01x03" H 8530 6866 50  0000 C CNN
F 2 "footprint:PinSocket_1x03_P2.54mm_Vertical" H 8610 7100 50  0001 C CNN
F 3 "~" H 8610 7100 50  0001 C CNN
	1    8610 7100
	-1   0    0    1   
$EndComp
Wire Wire Line
	8810 7100 9310 7100
Text Label 9310 7100 2    50   ~ 0
GND
Wire Wire Line
	8810 7200 9310 7200
Text Label 9310 7200 2    50   ~ 0
servo_power
Wire Notes Line
	6820 5970 9530 5970
Wire Notes Line
	9530 5970 9530 7430
Wire Notes Line
	9530 7430 6820 7430
Wire Notes Line
	6820 7430 6820 5970
Wire Wire Line
	13010 4750 12840 4750
Text Label 12840 4750 0    50   ~ 0
SDI
Wire Wire Line
	13010 4650 12840 4650
Wire Wire Line
	13010 4550 12840 4550
Wire Wire Line
	13010 4850 12840 4850
Text Label 12840 4850 0    50   ~ 0
SDO
Text Label 12840 4650 0    50   ~ 0
SCK
Text Label 12840 4550 0    50   ~ 0
CS_X
Text Label 12820 5000 0    50   ~ 0
CS_Y
Wire Wire Line
	13010 5000 12820 5000
Text Label 12760 5950 0    50   ~ 0
CS_Z1
Wire Wire Line
	12760 5950 13010 5950
Text Label 12770 5700 0    50   ~ 0
CS_E
Wire Wire Line
	12770 5700 13010 5700
Text Label 12760 5850 0    50   ~ 0
CS_Z2
Wire Wire Line
	13010 5850 12760 5850
Text Label 7030 6360 3    50   ~ 0
servo_power
Text Label 7130 6360 3    50   ~ 0
GND
$Comp
L Connector_Generic:Conn_01x02 J22
U 1 1 5BC20FC5
P 7130 7090
F 0 "J22" V 7120 7150 50  0000 L CNN
F 1 "Conn_01x02" V 7230 6820 50  0000 L CNN
F 2 "footprint:PinSocket_1x02_P2.54mm_Vertical" H 7130 7090 50  0001 C CNN
F 3 "~" H 7130 7090 50  0001 C CNN
	1    7130 7090
	0    1    1    0   
$EndComp
Wire Wire Line
	7030 6890 7030 6360
Wire Wire Line
	7130 6890 7130 6360
$EndSCHEMATC
